package main

import (
	"fmt"
	"github.com/openinx/yousql/sqlparser"
)

func main() {
	sql := "select * from dbinstance , vm where dbinstance.dbInstanceId = vm.dbInstanceId   order by dbinstance.dbInstanceId desc"
	fmt.Printf("raw sql: %s\n", sql)
	tree, err := sqlparser.Parse(sql)
	if err != nil {
		fmt.Printf(err.Error())
	}
	out := sqlparser.String(tree)
	fmt.Printf("rewrite: %s\n", out)
}
