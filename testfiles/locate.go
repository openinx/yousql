// Copyright 2013, Google Inc. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package testfiles locates test files within the Vitess directory tree.
package testfiles

import (
	"fmt"
	"path"
	"path/filepath"
	"runtime"
)

func Glob(pattern string) []string {
	_, filename, _, ok := runtime.Caller(1)
	if !ok {
		return []string{}
	}
	resolved := path.Join(path.Dir(filename), "..", pattern)
	fmt.Printf("resolved: %s\n", resolved)
	out, err := filepath.Glob(resolved)
	if err != nil {
		panic(err)
	}
	return out
}
