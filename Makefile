all: build test

build:
	go build -o bin/main cmd/main.go

test:
	go test ./...
